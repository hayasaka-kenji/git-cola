const fs = require('fs');
const gulp = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const flexBugsFixes = require('postcss-flexbugs-fixes');
const cssWring = require('csswring');
const ejs = require('gulp-ejs');
const htmlmin = require('gulp-htmlmin');
const imagemin = require('gulp-imagemin');
const imageminPngquant = require('imagemin-pngquant');
const imageminMozjpeg = require('imagemin-mozjpeg');
const browserSync = require('browser-sync').create();
const ftp = require('vinyl-ftp');
const fancyLog = require('fancy-log');

// autoprefixerOption
const autoprefixerOption = {
    grid: true
}

const postcssOption = [
    flexBugsFixes,
    autoprefixer(autoprefixerOption),
    cssWring
];

// task sass
gulp.task('sass', () => {
    // console.log(gulp.src('./src/sass/common.scss'));
    return gulp.src('./src/sass/common.scss')
        .pipe(sass())
        .pipe(postcss(postcssOption))
        .pipe(gulp.dest('./dist'))
});

// ejsのデータ読み込み設定
const configJsonData = fs.readFileSync('./src/ejs/config.json');
const configObj = JSON.parse(configJsonData);
const ejsDataOption = {
    config: configObj
}

// ejsのコンパイル設定
const ejsSettingOption = {
    ext: '.html'
}

// htmlminの設定
const htmlminOption = {
    collapseWhitespace: true
}

// task ejs
gulp.task('ejs', () =>{
    return gulp
        .src('./src/ejs/*.ejs')
        .pipe(ejs(ejsDataOption, {}, ejsSettingOption))
        .pipe(htmlmin(htmlminOption))
        .pipe(gulp.dest('./dist'))
})

// imageminOption
const imageminOption = [
    imageminPngquant({ quality: '65-80' }),
    imageminMozjpeg({ quality: 80 }),
    imagemin.gifsicle(),
    imagemin.jpegtran(),
    imagemin.optipng(),
    imagemin.svgo()
]

// task imagemin 
gulp.task('imagemin', () => {
    return gulp
        .src('src/images/*')
        .pipe(imagemin(imageminOption))
        .pipe(gulp.dest('dist/images'))
})

// browserSyncOption
const browserSyncOption = {
    server: './dist'
}

// task browser-sync
gulp.task('serve', (done) => {
    browserSync.init(browserSyncOption)
    done()
})

// task ftp

gulp.task('ftp', () => {
    const ftpConfig = {
        host: '******.jp',
        user: '******',
        password: '******',
        log: fancyLog
}

const connect = ftp.create(ftpConfig);
const ftpUploadFiles = './dist/**/*';

const ftpUploadConfig = {
    buffer: false
}

const remoteDistDir = 'public_html';

return gulp.src(ftpUploadFiles, ftpUploadConfig)
    .pipe(connect.newer(remoteDistDir))
    .pipe(connect.dest(remoteDistDir))
})

// task watch
gulp.task('watch', (done) => {
    const browserReload = (done) => {
        browserSync.reload()
        done()
    }
    gulp.watch('./dist/**/*', browserReload)
})
gulp.task('watch', () => {
    gulp.watch('./src/sass/**/*.scss', gulp.series('sass'))
    gulp.watch('./src/ejs/**/*.ejs', gulp.series('ejs'))
})

// default
gulp.task('default', gulp.series('serve', 'watch'))
